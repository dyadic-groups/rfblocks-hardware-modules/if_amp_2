# A Wideband, High Linearity IF Amplifier.

Although not commonly considered for IF amplifiers, wideband operational
amplifiers can offer considerable performance advantages at the lower IF (or HF)
over standard AC coupled MMIC amplifiers.

## Features

- Flat 20 dB gain up to ~100MHz, -3 dB at ~70 MHz for 27 dB gain.
- High linearity and low distortion up to ~50MHz:
  OIP3 > 25 dB and HD3 < -70 dBc at 50 MHz
- Customizable gain.
- Good input and output impedance matching:
  $`\textrm{ S}_{11}`$ and $`\textrm{ S}_{22}`$ < -22 dB up to 100 MHz.
- Excellent reverse isolation: < -70 dB up to 100 MHz.
- Single supply design and low quiescent supply current: 22 mA at 10V.

## Documentation

Full documentation for the amplifier is available at
[RF Blocks](https://rfblocks.org/boards/OPA847-Amp.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
